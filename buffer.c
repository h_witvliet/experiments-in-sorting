#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "buffer.h"


// create a simple buffer for a number of strings. The program must keep track
// of all the pointers to the strings which are stored
// if the memory cannot be allocated, return a zero pointer
s_buf *simplebuffer_create (size_t size) 
{
    s_buf *buffer = (s_buf *) malloc(sizeof(s_buf));
    if (buffer == NULL)
        return NULL;
    buffer->size = size;
    buffer->data = malloc(size);
    if (buffer->data == NULL) {
        free(buffer);
        return NULL;
    } else {
        buffer->free = buffer->data;
        return buffer;
    }
}


// return the memory used by the buffer to the heap
void simplebuffer_free (s_buf *buffer)
{
    free(buffer->data);
    free(buffer);
}


// add a string to the buffer, as long as there is enough room. If the string
// doesn't fit, return a zero pointer
char *simplebuffer_add (s_buf *buffer, char *string) 
{
    if (strlen(string) >= buffer->size - (buffer->free - buffer->data) - 1)
        return NULL;
    strcpy(buffer->free, string);
    char *bufpointer = buffer->free;
    buffer->free += strlen(string) + 1;
    return bufpointer;
}

// return the amount of free space
size_t simplebuffer_getfreespace(s_buf *buffer)
{
    return buffer->size - (buffer->free - buffer->data);
}

// empty the buffer for reuse
void simplebuffer_empty (s_buf *buffer)
{
    buffer->free = buffer->data;
}


int main() {
    s_buf *mybuf = simplebuffer_create(1000000000);
    char *invoerstring1 = "string1";
    char *invoerstring2 = "nog een string";

    char *string1 = simplebuffer_add(mybuf, invoerstring1);
    char *string2 = simplebuffer_add(mybuf, invoerstring2);

    printf("%s\n", string1);
    printf("%s\n", string2);
    printf("free space: %ld\n", simplebuffer_getfreespace(mybuf));

    printf("size: %ld \n", mybuf->free - mybuf->data);

    simplebuffer_free(mybuf);
}
