typedef struct
{
    char *data;       // pointer to the beginning of the string data
    size_t size;      // total size (max) of the buffer
    char *free;       // pointer to the first free bufferspace
} s_buf;

// create a simple buffer for a number of strings. The program must keep track
// of all the pointers to the strings which are stored
// if the memory cannot be allocated, return a zero pointer
s_buf *simplebuffer_create (size_t size);

// return the memory used by the buffer to the heap
void simplebuffer_free (s_buf *buffer);

// add a string to the buffer, as long as there is enough room. If the string
// doesn't fit, return a zero pointer
char *simplebuffer_add (s_buf *buffer, char *string);

// return the amount of free space
size_t simplebuffer_getfreespace(s_buf *buffer);

// empty the buffer for reuse
void simplebuffer_empty (s_buf *buffer);

